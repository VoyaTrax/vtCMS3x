[![alt XOOPS CMS](http://voyatrax.eu/images/vt_logo_v2017.200-150.png)](http://voyatrax.eu)
# vtCMS3x
[![Software License](https://img.shields.io/badge/license-GPLv3-red.svg?style=plastic)](docs/license.txt)

is the development codname for this project. It is starting as a XOOPS 2.5.9-beta3, becuase this may be the last version done this way.

Version 2.6.x of XOOPS has a lot of changes coming, I may not like it for some reason and would like to keep things they were...

So this will be more or less a backup, along with some changes or fixes, addons, etc. that I do on most of my sites.

I will also create a repository of modules I usually include along with some that were nice but outdated. That will be anounced and linked to later.

## What's Changed or Added:

* xLanguage included
  * setup for English & German
* German included
* English (default)
* News module included
  * German & English
* Modded xBootstrap theme
* 2-3 Nice themes found on xoops.org (or links from there)
* FAQ module
* Newsletter module
* Sitemap module
* EU Cookie law conformity

## Original Source Info:

![alt XOOPS CMS](http://xoops.org/images/logoXoops4GithubRepository.png)
# XOOPS Core 2.5.x

[![Build Status](https://img.shields.io/travis/XOOPS/XoopsCore/master.svg?style=flat)](https://travis-ci.org/XOOPS/XoopsCore25)
[![Software License](https://img.shields.io/badge/license-GPL-brightgreen.svg?style=flat)](docs/license.txt)
[![Coverage Status](https://img.shields.io/scrutinizer/coverage/g/XOOPS/XoopsCore25.svg?style=flat)](https://scrutinizer-ci.com/g/XOOPS/XoopsCore25/code-structure)
[![Quality Score](https://img.shields.io/scrutinizer/g/XOOPS/XoopsCore25.svg?style=flat)](https://scrutinizer-ci.com/g/XOOPS/XoopsCore25)
[![Latest Version](https://img.shields.io/github/release/XOOPS/XoopsCore25.svg?style=flat)](https://github.com/XOOPS/XoopsCore25/releases)

> **Note:** This repository contains the core code of the XOOPS Core 2.5.x.
It's under development currently. If you want to build a web site using XOOPS, visit the main [XOOPS Web Site](http://xoops.org) for more information.

## Developer Installation

To participate in XOOPS development, fork this repository on GitHub.

Pull your fork to your development environment.

To install this version of XOOPS 2.5.x follow the instructions in this [XOOPS Installation Guide](https://www.gitbook.com/book/xoops/xoops-installation-guide/)

## Contributing

Thank you for considering contributing to the XOOPS Project. See [CONTRIBUTING](CONTRIBUTING.md) file.

When your changes are complete and tested, send us a Pull Request
on GitHub and we will take a look.
