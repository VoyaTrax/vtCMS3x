===================
text-blink.js howto
===================

1) include the file where you want blinking text. I added this to the news module.

		// Attempt to implement blinking text using javascript.
		// Found @ : https://en.wikipedia.org/wiki/Blink_element
		// Added by hyperclock
		include_once XOOPS_ROOT_PATH.'/class/js/text-blink.js';

2) Wherever you need blinking text just do:

		<blink>YOUR BLINKING TEXT HERE</blink>
		

That's it. 


If you add this to the header files it should be 
available for the entire site. Don't if that's a good idea todo.

-- hyperclock
