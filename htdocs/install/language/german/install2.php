<?php
// 
// _LANGCODE: en
// _CHARSET : UTF-8
// Translator: XOOPS Translation Team
// License
define('LICENSE_NOT_WRITEABLE', 'Lizenz ist %s . <br/><font style="colour:#ff0000">Make ../include/license.php beschreibbar</font>');
define('LICENSE_IS_WRITEABLE', 'Lizenz ist %s');
define('_INSTALL_WEBMASTER','Webmaster');
define('_INSTALL_WEBMASTERD','Webmaster dieser Seite');
define('_INSTALL_REGUSERS','Mitglieder');
define('_INSTALL_REGUSERSD','Registrierte Usergruppe');
define('_INSTALL_ANONUSERS','Gäste');
define('_INSTALL_ANONUSERSD','Anonyme Usergruppe');
/**
 * New Group types
 */
define('_INSTALL_BANNEDUSERS', 'gesperrte Users');
define('_INSTALL_BANNEDUSERSD', 'Usergruppe die gesperrt ist');
define('_INSTALL_MODERATORUSERS', 'Moderator');
define('_INSTALL_MODERATORUSERSD', 'Die Moderatoren dieser Seite');
define('_INSTALL_SUBMITTERUSERS', 'Autoren');
define('_INSTALL_SUBMITTERUSERSD', 'Diese Gruppe kann Artikel oder ähnliches schreiben');
define('_INSTALL_DEVELOPEUSERS', 'Entwickler');
define('_INSTALL_DEVELOPEUSERSD', 'Diese Gruppe kann Anpassungen der Websites vornehmen.');
define("_INSTALL_L165","Diese Seite ist wegen Wartungsarbeiten geschlossen! Bitte kommen Sie später nocheinmal wieder!");
define('_INSTALL_ANON','Gast');
define('_INSTALL_DISCLMR', 'Diese Seite übernimmt keinerlei Haftung für Schäden, die durch das System (die Internetseite) oder
die angebotenen Dateien entstehen. Alle Dateien sind auf Viren geprüft. Der User wird hiermit
darauf hingewiesen, selber die Dateien auf Viren und ähnliches zu prüfen.
Eine Garantie für die Sicherheit der Dateien kann nicht gegeben gegeben werden, da diese Dateien
meist nicht aus unserer Produktion stammen. Auch kann keine Garantie übernommen werden für die
Erreichbarkeit unseres Services und von Dateien. Das Herunterladen, die Installation und die
Verwendung der Programme die unter dieser Seite aufgeführt werden, erfolgt auf eigene Gefahr!
Der Betreiber übernimmt keine Gewährleistung oder Haftung für etwaige Schäden, Folgeschäden oder
Ausfälle, die entstehen können.

Erklärung: Bei unseren externen Links handelt es sich um eine subjektive Auswahl von Verweisen
auf andere Internetseiten. Für den Inhalt dieser Seiten sind die jeweiligen Betreiber / Verfasser
selbst verantwortlich und haftbar. Von etwaigen illegalen, persönlichkeitsverletzenden,
moralisch oder ethisch anstößigen Inhalten distanzieren wir uns in aller Deutlichkeit.
Bitte informieren Sie uns, wenn wir auf ein solches Angebot linken sollten.

Diese Seite ist als Inhaltsanbieter für die eigenen Inhalte, die er zur Nutzung bereithält, nach
den allgemeinen Gesetzen verantwortlich. Von diesen eigenen Inhalten sind Querverweise (Links) auf
die von anderen Anbietern bereitgehaltenen Inhalte zu unterscheiden. Für diese fremden Inhalte ist
diese Seite nur dann verantwortlich, wenn von ihnen (d. h. auch von einem rechtswidrigen bzw.
strafbaren Inhalt) positive Kenntnis vorliegt und es technisch möglich und zumutbar ist, deren
Nutzung zu verhindern (§5 Abs.2 TDG). Bei Links handelt es sich allerdings stets um lebende
(dynamische) Verweise. Diese Seite hat bei der erstmaligen Verknüpfung zwar den fremden Inhalt
daraufhin überprüft, ob durch ihn eine mögliche zivilrechtliche oder strafrechtliche
Verantwortlichkeit ausgelöst wird. Der Inhaltsanbieter ist aber nach dem TDG nicht dazu verpflichtet,
die Inhalte, auf die er in seinem Angebot verweist, ständig auf Veränderungen zu überprüfen, die
eine Verantwortlichkeit neu begründen könnten. Erst wenn er feststellt oder von anderen darauf
hingewiesen wird, dass ein konkretes Angebot, zu dem er einen Link bereitgestellt hat, eine
zivil- oder strafrechtliche Verantwortlichkeit auslöst, wird er den Verweis auf dieses Angebot
aufheben, soweit ihm dies technisch möglich und zumutbar ist. Die technische Möglichkeit und
Zumutbarkeit wird nicht dadurch beeinflusst, dass auch nach Unterbindung des Zugriffs von dieser
Seite, von anderen Servern aus auf das rechtswidrige oder strafbare Angebot zugegriffen werden kann.
Salvatorische Klausel: Sollte aus irgendwelchen Gründen eine der vorstehenden Bedingungen ungültig
sein, so wird die Wirksamkeit der anderen Bestimmungen davon nicht berührt. Der Download von
Programmen von oben genannter Seite erfolgt auf eigene Gefahr. Sämtliche hier angebotenen Downloads
werden keiner Virenprüfung unterzogen. Eine Haftung seitens dieser Seite für Schäden und
Beeinträchtigungen durch Computerviren ist ausgeschlossen. Schadenersatzansprüche sind
ausgeschlossen. Dies gilt auch für Ansprüche auf Ersatz von Folgeschäden wie Datenverlust.');

// TERMS,PRIVACY & IMPRINT - hyperclock
#define("_INSTALL_IMPRINT","Impressum");
#define("_INSTALL_PRIVACY","Datenschutzerklärung");
#define("_INSTALL_TERMS","Nutzungsbedingungen");

// About us - hyperclock
#define("_INSTALL_ABOUT","Über Uns");

// DISCLAIMER - hyperclock
#define("_INSTALL_DISCLAIMER","Haftungsausschluss");

################
// IMPRINT - hyperclock
define('_INSTALL_IMPRINT','[b]Angaben gemäß § 5 TMG:[/b]
Max Musterman
Muster Firma
Muster str. 0
00000 Musterstadt

[b]Kontakt:[/b]
Telefon:  +4900001234567
Telefax:  +4900001234567
E-Mail:   info@example.com


[b]Verantwortlich für den Inhalt nach § 55 Abs. 2 RStV:[/b]
Max Musterman
Muster str. 0
00000 Musterstadt
Country


[b]Quellenangaben für die verwendeten Bilder und Grafiken:[/b]
http:// N/A  -  Photograf: N/A

Quelle: [url=http://www.e-recht24.de]http://www.e-recht24.de[/url]');

// TERMS - hyperclock
define('_INSTALL_TERMS','[b]§1 Geltungsbereich[/b]
Für die Nutzung dieser Website gelten im Verhältnis zwischen dem Nutzer und dem Betreiber der Seite (im Folgenden: Anbieter) die folgenden Nutzungsbedingungen. Die Nutzung des Forums und der Communityfunktionen ist nur zulässig, wenn der Nutzer diese Nutzungsbedingungen akzeptiert.



[b]§2 ]Registrierung, Teilnahme, Mitgliedschaft in der Community[/b]
(1) Voraussetzung für die Nutzung des Forums und der Community ist eine vorherige Registrierung.  Mit der erfolgreichen Registrierung wird der Nutzer Mitglied der Community.
(2) Es besteht kein Anspruch auf eine Mitgliedschaft.
(3) Der Nutzer darf seinen Zugang nicht Dritten zur Nutzung überlassen. Der Nutzer ist verpflichtet, seine Zugangsdaten geheim zu halten und vor dem Zugriff Dritter zu schützen.



[b]§3 Leistungen des Anbieters[/b]
(1) Der Anbieter gestattet dem Nutzer, im Rahmen dieser Nutzungsbedingungen Beiträge auf seiner Webseite zu veröffentlichen. Der Anbieter stellt den Nutzern dazu im Rahmen seiner technischen und wirtschaftlichen Möglichkeiten unentgeltlich ein Diskussionsforum mit Communityfunktionen zur Verfügung. Der Anbieter ist bemüht, seinen Dienst verfügbar zu halten. Der Anbieter übernimmt keine darüber hinausgehenden Leistungspflichten. Insbesondere besteht kein Anspruch des Nutzers auf eine ständige Verfügbarkeit des Dienstes.
(2) Der Anbieter übernimmt keine Gewähr für die Richtigkeit, Vollständigkeit, Verlässlichkeit, Aktualität und Brauchbarkeit der bereit gestellten Inhalte.



[b]§4 Haftungsausschluss[/b]
(1) Schadensersatzansprüche des Nutzers sind ausgeschlossen, soweit nachfolgend nichts anderes bestimmt ist. Der vorstehende Haftungsausschluss gilt auch zugunsten der gesetzlichen Vertreter und Erfüllungsgehilfen des Anbieters, sofern der Nutzer Ansprüche gegen diese geltend macht.
(2) Von dem in Absatz 1 bestimmten Haftungsausschluss ausgenommen sind Schadensersatzansprüche aufgrund einer Verletzung des Lebens, des Körpers, der Gesundheit und Schadensersatzansprüche aus der Verletzung wesentlicher Vertragspflichten. Wesentliche Vertragspflichten sind solche, deren Erfüllung zur Erreichung des Ziels des Vertrags notwendig ist. Von dem Haftungsausschluss ebenfalls ausgenommen ist die Haftung für Schäden, die auf einer vorsätzlichen oder grob fahrlässigen Pflichtverletzung des Anbieters, seiner gesetzlichen Vertreter oder Erfüllungsgehilfen beruhen.



[b]§5 Pflichten des Nutzers[/b]
(1) Der Nutzer verpflichtet sich gegenüber dem Anbieter, keine Beiträge zu veröffentlichen, die gegen die guten Sitten oder geltendes Recht verstoßen. Der Nutzer verpflichtet sich insbesondere dazu, keine Beiträge zu veröffentlichen,
[ul]
  [li]deren Veröffentlichung einen Straftatbestand erfüllt oder eine Ordnungswidrigkeit darstellt,[/li]
  [li]die gegen das Urheberrecht, Markenrecht oder Wettbewerbsrecht verstoßen,[/li]
  [li]die gegen das Rechtsdienstleistungsgesetz verstoßen,[/li]
  [li]die beleidigenden, rassistischen, diskriminierenden oder pornographischen Inhalt haben,[/li]
  [li]die Werbung enthalten.[/li]
[/ul]
(2) Bei einem Verstoß gegen die Verpflichtung aus Absatz 1 ist der Anbieter berechtigt, die entsprechenden Beiträge abzuändern oder zu löschen und den Zugang des Nutzers zu sperren. Der Nutzer ist verpflichtet, dem Anbieter den durch die Pflichtverletzung entstandenen Schaden zu ersetzen.
(3) Der Anbieter hat das Recht, Beiträge und Inhalte zu löschen, wenn diese einen Rechtsverstoß enthalten könnten.
(4) Der Anbieter hat gegen den Nutzer einen Anspruch auf Freistellung von Ansprüchen Dritter, die diese wegen der Verletzung eines Rechts durch den Nutzer geltend machen. Der Nutzer verpflichtet sich, den Anbieter bei der Abwehr derartiger Ansprüche zu unterstützen. Der Nutzer ist außerdem verpflichtet, die Kosten einer angemessenen Rechtsverteidigung des Anbieters zu tragen.



[b]§6 Übertragung von Nutzungsrechten[/b]
(1) Das Urheberrecht für die eingestellten Beiträge verbleibt beim jeweiligen Nutzer. Der Nutzer räumt dem Anbieter mit dem Einstellen seines Beitrags in das Forum jedoch das Recht ein, den Beitrag dauerhaft auf seiner Webseite zum Abruf bereitzuhalten und öffentlich zugänglich zu machen. Der Anbieter hat das Recht, Beiträge innerhalb seiner Webseite zu verschieben und mit anderen Inhalten zu verbinden.
(2) Der Nutzer hat gegen den Anbieter keinen Anspruch auf Löschung oder Berichtigung von ihm erstellter Beiträge.


[b]§7 Beendigung der Mitgliedschaft[/b]
(1) Der Nutzer kann seine Mitgliedschaft durch eine entsprechende Erklärung gegenüber dem Anbieter ohne Einhaltung einer Frist beenden. Auf Verlangen wird der Anbieter daraufhin den Zugang des Nutzers sperren.
(2) Der Anbieter ist berechtigt, die Mitgliedschaft eines Nutzers unter Einhaltung einer Frist von 2 Wochen zum Monatsende zu kündigen. 
(3) Bei Vorliegen eines wichtigen Grundes ist der Anbieter berechtigt, den Zugang des Nutzers sofort zu sperren und die Mitgliedschaft ohne Einhaltung einer Frist zu kündigen.
(4) Der Anbieter ist nach Beendigung der Mitglieschaft berechtigt, den Zugang des Nutzers zu sperren. Der Anbieter ist berechtigt aber nicht verpflichtet, im Falle der Beendigung der Mitgliedschaft die vom Nutzer erstellten Inhalte zu löschen. Ein Anspruch des Nutzers auf Überlassung der erstellten Inhalte wird ausgeschlossen.


[b]§8 Änderung oder Einstellung des Angebots[/b]
(1) Der Anbieter ist berechtigt, Änderungen an seinem Dienst vorzunehmen. 
(2) Der Anbieter ist berechtigt, seinen Dienst unter Einhaltung einer Ankündigungsfrist von 2 Wochen zu beenden. Im Falle der Beendigung seines Dienstes ist der Anbieter berechtigt aber nicht verpflichtet, die von den Nutzern erstellten Inhalte zu löschen.


[b]§9 Rechtswahl[/b]
Auf die vertraglichen Beziehungen zwischen dem Anbieter und dem Nutzer findet das Recht der Bundesrepublik Deutschland Anwendung. Von dieser Rechtswahl ausgenommen sind die zwingenden Verbraucherschutzvorschriften des Landes, in dem der Nutzer seinen gewöhnlichen Aufenthalt hat. 


[i]Quelle: [url=http://www.kluge-recht.de/muster-nutzungsbedingungen-forum.php]Nutzungsbedingungen Internet-Forum[/url][/i]');

// PRIVACY - hyperclock
define('_INSTALL_PRIVACY','[b]Datenschutz[/b]
Die Betreiber dieser Seiten nehmen den Schutz Ihrer persönlichen Daten sehr ernst. Wir behandeln Ihre personenbezogenen Daten vertraulich und entsprechend der gesetzlichen Datenschutzvorschriften sowie dieser Datenschutzerklärung.
Die Nutzung unserer Webseite ist in der Regel ohne Angabe personenbezogener Daten möglich. Soweit auf unseren Seiten personenbezogene Daten (beispielsweise Name, Anschrift oder E-Mail-Adressen) erhoben werden, erfolgt dies, soweit möglich, stets auf freiwilliger Basis. Diese Daten werden ohne Ihre ausdrückliche Zustimmung nicht an Dritte weitergegeben.
Wir weisen darauf hin, dass die Datenübertragung im Internet (z.B. bei der Kommunikation per E-Mail) Sicherheitslücken aufweisen kann. Ein lückenloser Schutz der Daten vor dem Zugriff durch Dritte ist nicht möglich.


[b]Datenschutzerklärung für die Nutzung von etracker[/b]
Unsere Webseite nutzt den Analysedienst etracker. Anbieter ist die etracker GmbH, Erste Brunnenstraße 1 20459 Hamburg Germany. 
Aus den Daten können unter einem Pseudonym Nutzungsprofile erstellt werden. Dazu können Cookies eingesetzt werden. Bei Cookies handelt es sich um kleine Textdateien, die lokal im Zwischenspeicher Ihres Internet-Browsers gespeichert werden. Die Cookies ermöglichen es, Ihren Browser wieder zu erkennen. Die mit den etracker-Technologien erhobenen Daten werden ohne die gesondert erteilte Zustimmung des Betroffenen nicht genutzt, Besucher unserer Website persönlich zu identifizieren und werden nicht mit personenbezogenen Daten über den Träger des Pseudonyms zusammengeführt.
Der Datenerhebung und -speicherung können Sie jederzeit mit Wirkung für die Zukunft widersprechen. Um einer Datenerhebung und -speicherung Ihrer Besucherdaten für die Zukunft zu widersprechen, können Sie unter nachfolgendem Link ein Opt-Out-Cookie von etracker beziehen, dieser bewirkt, dass zukünftig keine Besucherdaten Ihres Browsers bei etracker erhoben und gespeichert werden: [url=http://www.etracker.de/privacy?et=V23Jbb]http://www.etracker.de/privacy?et=V23Jbb[/url]
Dadurch wird ein Opt-Out-Cookie mit dem Namen "cntcookie" von etracker gesetzt. Bitte l&oouml;schen diesen Cookie nicht, solange Sie Ihren Widerspruch aufrecht erhalten möchten. Weitere Informationen finden Sie in den Datenschutzbestimmungen von etracker: [url=http://www.etracker.com/de/datenschutz.html]http://www.etracker.com/de/datenschutz.html[/url]


[b]Datenschutzerklärung für die Nutzung von Facebook-Plugins (Like-Button)[/b]
Auf unseren Seiten sind Plugins des sozialen Netzwerks Facebook, Anbieter Facebook Inc., 1 Hacker Way, Menlo Park, California 94025, USA, integriert. Die Facebook-Plugins erkennen Sie an dem Facebook-Logo oder dem "Like-Button" ("Gefällt mir") auf unserer Seite. Eine Übersicht über die Facebook-Plugins finden Sie hier: [url=http://developers.facebook.com/docs/plugins/]http://developers.facebook.com/docs/plugins/[/url].
Wenn Sie unsere Seiten besuchen, wird über das Plugin eine direkte Verbindung zwischen Ihrem Browser und dem Facebook-Server hergestellt. Facebook erhält dadurch die Information, dass Sie mit Ihrer IP-Adresse unsere Seite besucht haben. Wenn Sie den Facebook "Like-Button" anklicken während Sie in Ihrem Facebook-Account eingeloggt sind, können Sie die Inhalte unserer Seiten auf Ihrem Facebook-Profil verlinken. Dadurch kann Facebook den Besuch unserer Seiten Ihrem Benutzerkonto zuordnen. Wir weisen darauf hin, dass wir als Anbieter der Seiten keine Kenntnis vom Inhalt der übermittelten Daten sowie deren Nutzung durch Facebook erhalten. Weitere Informationen hierzu finden Sie in der Datenschutzerklärung von Facebook unter [url=http://de-de.facebook.com/policy.php]http://de-de.facebook.com/policy.php[/url].
Wenn Sie nicht wünschen, dass Facebook den Besuch unserer Seiten Ihrem Facebook- Nutzerkonto zuordnen kann, loggen Sie sich bitte aus Ihrem Facebook-Benutzerkonto aus.


[b]Datenschutzerklärung für die Nutzung von Google Analytics[/b]
Diese Website nutzt Funktionen des  Webanalysedienstes Google Analytics. Anbieter ist die Google Inc. 1600 Amphitheatre Parkway Mountain View, CA 94043, USA. Google Analytics verwendet sog. "Cookies". Das sind Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch Sie ermöglichen. Die durch den Cookie erzeugten Informationen über Ihre Benutzung dieser Website werden in der Regel an einen Server von Google in den USA übertragen und dort gespeichert.
Im Falle der Aktivierung der IP-Anonymisierung auf dieser Webseite wird Ihre IP-Adresse von Google jedoch innerhalb von Mitgliedstaaten der Europäischen Union oder in anderen Vertragsstaaten des Abkommens über den Europäischen Wirtschaftsraum zuvor gekürzt. Nur in Ausnahmefällen wird die volle IP-Adresse an einen Server von Google in den USA übertragen und dort gekürzt. Im Auftrag des Betreibers dieser Website wird Google diese Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um Reports über die Websiteaktivitäten zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen gegenüber dem Websitebetreiber zu erbringen. Die im Rahmen von Google Analytics von Ihrem Browser übermittelte IP-Adresse wird nicht mit anderen Daten von Google zusammengeführt.
Sie können die Speicherung der Cookies durch eine entsprechende Einstellung Ihrer Browser-Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website vollumfänglich werden nutzen können. Sie können darüber hinaus die Erfassung der durch das Cookie erzeugten und auf Ihre Nutzung der Website bezogenen Daten (inkl. Ihrer IP-Adresse) an Google sowie die Verarbeitung dieser Daten durch Google verhindern, indem sie das unter dem folgenden Link verfügbare Browser-Plugin herunterladen und installieren: [url=http://tools.google.com/dlpage/gaoptout?hl=de ]http://tools.google.com/dlpage/gaoptout?hl=de[/url]


[b]Datenschutzerklärung für die Nutzung von Google +1[/b]
Unsere Seiten nutzen Funktionen von Google +1. Anbieter ist die Google Inc. 1600 Amphitheatre Parkway Mountain View, CA 94043,  USA.
Erfassung und Weitergabe von Informationen: Mithilfe der Google +1-Schaltfläche können Sie Informationen weltweit veröffentlichen. über die Google +1-Schaltfläche erhalten Sie und andere Nutzer personalisierte Inhalte von Google und unseren Partnern. Google speichert sowohl die Information, dass Sie für einen Inhalt +1 gegeben haben, als auch Informationen über die Seite, die Sie beim Klicken auf +1 angesehen haben. Ihre +1 können als Hinweise zusammen mit Ihrem Profilnamen und Ihrem Foto in Google-Diensten, wie etwa in Suchergebnissen oder in Ihrem Google-Profil, oder an anderen Stellen auf Websites und Anzeigen im Internet eingeblendet werden. Google zeichnet Informationen über Ihre +1-Aktivitäten auf, um die Google-Dienste für Sie und andere zu verbessern. Um die Google +1-Schaltfläche verwenden zu können, benötigen Sie ein weltweit sichtbares, öffentliches Google-Profil, das zumindest den für das Profil gewählten Namen enthalten muss. Dieser Name wird in allen Google-Diensten verwendet. In manchen Fällen kann dieser Name auch einen anderen Namen ersetzen, den Sie beim Teilen von Inhalten über Ihr Google-Konto verwendet haben. Die Identität Ihres Google- Profils kann Nutzern angezeigt werden, die Ihre E-Mail-Adresse kennen oder über andere identifizierende Informationen von Ihnen verfügen.
Verwendung der erfassten Informationen: Neben den oben erläuterten Verwendungszwecken werden die von Ihnen bereitgestellten Informationen gemäß den geltenden Google-Datenschutzbestimmungen genutzt. Google veröffentlicht möglicherweise zusammengefasste Statistiken über die +1-Aktivitäten der Nutzer bzw. gibt diese an Nutzer und Partner weiter, wie etwa Publisher, Inserenten oder verbundene Websites.


[b]Datenschutzerklärung für die Nutzung von Instagram[/b]
Auf unseren Seiten sind Funktionen des Dienstes Instagram eingebunden. Diese Funktionen werden angeboten durch die Instagram Inc., 1601 Willow Road, Menlo Park, CA, 94025, USA integriert. Wenn Sie in Ihrem Instagram - Account eingeloggt sind können Sie durch Anklicken des Instagram - Buttons die Inhalte unserer Seiten mit Ihrem Instagram - Profil verlinken. Dadurch kann Instagram den Besuch unserer Seiten Ihrem Benutzerkonto zuordnen. Wir weisen darauf hin, dass wir als Anbieter der Seiten keine Kenntnis vom Inhalt der übermittelten Daten sowie deren Nutzung durch Instagram erhalten.
Weitere Informationen hierzu finden Sie in der Datenschutzerklärung von Instagram: [url=http:/instagram.com/about/legal/privacy/]http://instagram.com/about/legal/privacy/[/url]


[b]Datenschutzerklärung für die Nutzung von LinkedIn[/b]
Unsere Website nutzt Funktionen des Netzwerks LinkedIn. Anbieter ist die LinkedIn Corporation, 2029 Stierlin Court, Mountain View, CA 94043, USA. Bei jedem Abruf einer unserer Seiten, die Funktionen von LinkedIn enthält, wird eine Verbindung zu Servern von LinkedIn aufbaut. LinkedIn wird darüber informiert, dass Sie unsere Internetseiten mit Ihrer IP-Adresse besucht haben. Wenn Sie den "Recommend-Button" von LinkedIn anklicken und in Ihrem Account bei LinkedIn eingeloggt sind, ist es LinkedIn möglich, Ihren Besuch auf unserer Internetseite Ihnen und Ihrem Benutzerkonto zuzuordnen. Wir weisen darauf hin, dass wir als Anbieter der Seiten keine Kenntnis vom Inhalt der übermittelten Daten sowie deren Nutzung durch LinkedIn haben.
Weitere Informationen hierzu finden Sie in der Datenschutzerklärung von LinkedIn unter: [url=https://www.linkedin.com/legal/privacy-policy]https://www.linkedin.com/legal/privacy-policy[/url]


[b]Datenschutzerklärung für die Nutzung von Pinterest[/b]
Auf unserer Seite verwenden wir Social Plugins des sozialen Netzwerkes Pinterest, das von der Pinterest Inc., 635 High Street, Palo Alto, CA, 94301, USA ("Pinterest") betrieben wird. Wenn Sie eine Seite aufrufen die ein solches Plugin enthält, stellt Ihr Browser eine direkte Verbindung zu den Servern von Pinterest her. Das Plugin übermittelt dabei Protokolldaten an den Server von Pinterest in die USA. Diese Protokolldaten enthalten möglicherweise Ihre IP-Adresse, die Adresse der besuchten Websites, die ebenfalls Pinterest-Funktionen enthalten, Art und Einstellungen des Browsers, Datum und Zeitpunkt der Anfrage, Ihre Verwendungsweise von Pinterest sowie Cookies.
Weitere Informationen zu Zweck, Umfang und weiterer Verarbeitung und Nutzung der Daten durch Pinterest sowie Ihre diesbezüglichen Rechte und Möglichkeiten zum Schutz Ihrer Privatsphäre finden Sie in den den Datenschutzhinweisen von Pinterest: [url=https://about.pinterest.com/de/privacy-policy]https://about.pinterest.com/de/privacy-policy[/url]


[b]Datenschutzerklärung für die Nutzung von Tumblr[/b]
Unsere Seiten nutzen Schaltflächen des Dienstes Tumblr. Anbieter ist die Tumblr, Inc., 35 East 21st St, 10th Floor, New York, NY 10010, USA. Diese Schaltflächen ermöglichen es Ihnen, einen Beitrag oder eine Seite bei Tumblr zu teilen oder dem Anbieter bei Tumblr zu folgen. Wenn Sie eine unserer Webseiten mit Tumblr-Button aufrufen, baut der Browser eine direkte Verbindung mit den Servern von Tumblr auf. Wir haben keinen Einfluss auf den Umfang der Daten, die Tumblr mit Hilfe dieses Plugins erhebt und übermittelt. Nach aktuellem Stand werden die IP-Adresse des Nutzers sowie die URL der jeweiligen Webseite übermittelt.
Weitere Informationen hierzu finden sich in der Datenschutzerklärung von Tumblr unter [url=http://www.tumblr.com/policy/de/privacy"]http://www.tumblr.com/policy/de/privacy[/url].

[b]Datenschutzerklärung für die Nutzung von Twitter[/b]
[Auf unseren Seiten sind Funktionen des Dienstes Twitter eingebunden. Diese Funktionen werden angeboten durch die Twitter Inc., 1355 Market Street, Suite 900, San Francisco, CA 94103, USA. Durch das Benutzen von Twitter und der Funktion "Re-Tweet" werden die von Ihnen besuchten Webseiten mit Ihrem Twitter-Account verknüpft und anderen Nutzern bekannt gegeben. Dabei werden auch Daten an Twitter übertragen. Wir weisen darauf hin, dass wir als Anbieter der Seiten keine Kenntnis vom Inhalt der übermittelten Daten sowie deren Nutzung durch Twitter erhalten. Weitere Informationen hierzu finden Sie in der Datenschutzerklärung von Twitter unter [url=http://twitter.com/privacy"]http://twitter.com/privacy[/url].
Ihre Datenschutzeinstellungen bei Twitter können Sie in den Konto-Einstellungen unter [url=http://twitter.com/account/settings]http://twitter.com/account/settings[/url] ändern.


[b]Datenschutzerklärung für die Nutzung von Xing[/b]
Unsere Webseite nutzt Funktionen des Netzwerks XING. Anbieter ist die XING AG, Dammtorstraße 29-32, 20354 Hamburg, Deutschland. Bei jedem Abruf einer unserer Seiten, die Funktionen von Xing enthält, wird eine Verbindung zu Servern von Xing hergestellt. Eine Speicherung von personenbezogenen Daten erfolgt dabei nach unserer Kenntnis nicht. Insbesondere werden keine IP-Adressen gespeichert oder das Nutzungsverhalten ausgewertet.
Weitere Information zum Datenschutz und dem Xing Share-Button finden Sie in der Datenschutzerklärung von Xing unter [url=https://www.xing.com/app/share?op=data_protection]https://www.xing.com/app/share?op=data_protection[/url]


[b]Auskunft, Löschung, Sperrung[/b]
Sie haben jederzeit das Recht auf unentgeltliche Auskunft über Ihre gespeicherten personenbezogenen Daten, deren Herkunft und Empfänger und den Zweck der Datenverarbeitung sowie ein Recht auf Berichtigung, Sperrung oder Löschung dieser Daten. Hierzu sowie zu weiteren Fragen zum Thema personenbezogene Daten können Sie sich jederzeit unter der im Impressum angegebenen Adresse an uns wenden.


[b]Server-Log-Files[/b]
Der Provider der Seiten erhebt und speichert automatisch Informationen in so genannten Server-Log Files, die Ihr Browser automatisch an uns übermittelt. Dies sind:
  [ul]
    [li]Browsertyp/ Browserversion[/li]
    [li]verwendetes Betriebssystem[/li]
    [li]Referrer URL[/li]
    [li]Hostname des zugreifenden Rechners[/li]
    [li]Uhrzeit der Serveranfrage[/li]
  [/ul]
Diese Daten sind nicht bestimmten Personen zuordenbar. Eine Zusammenführung dieser Daten mit anderen Datenquellen wird nicht vorgenommen. Wir behalten uns vor, diese Daten nachträglich zu prüfen, wenn uns konkrete Anhaltspunkte für eine rechtswidrige Nutzung bekannt werden.


[b]Cookies[/b]
Die Internetseiten verwenden teilweise so genannte Cookies. Cookies richten auf Ihrem Rechner keinen Schaden an und enthalten keine Viren. Cookies dienen dazu, unser Angebot nutzerfreundlicher, effektiver und sicherer zu machen. Cookies sind kleine Textdateien, die auf Ihrem Rechner abgelegt werden und die Ihr Browser speichert.
Die meisten der von uns verwendeten Cookies sind so genannte „Session-Cookies“. Sie werden nach Ende Ihres Besuchs automatisch gelöscht. Andere Cookies bleiben auf Ihrem Endgerät gespeichert, bis Sie diese löschen. Diese Cookies ermöglichen es uns, Ihren Browser beim nächsten Besuch wiederzuerkennen.
Sie können Ihren Browser so einstellen, dass Sie über das Setzen von Cookies informiert werden und Cookies nur im Einzelfall erlauben, die Annahme von Cookies für bestimmte Fälle oder generell ausschließen sowie das automatische Löschen der Cookies beim Schließen des Browser aktivieren. Bei der Deaktivierung von Cookies kann die Funktionalität dieser Website eingeschränkt sein.



[b]Kontaktformular[/b]
Wenn Sie uns per Kontaktformular Anfragen zukommen lassen, werden Ihre Angaben aus dem Anfrageformular inklusive der von Ihnen dort angegebenen Kontaktdaten zwecks Bearbeitung der Anfrage und für den Fall von Anschlussfragen bei uns gespeichert. Diese Daten geben wir nicht ohne Ihre Einwilligung weiter.



[b]Widerspruch Werbe-Mails[/b]
Der Nutzung von im Rahmen der Impressumspflicht veröffentlichten Kontaktdaten zur Übersendung von nicht ausdrücklich angeforderter Werbung und Informationsmaterialien wird hiermit widersprochen. Die Betreiber der Seiten behalten sich ausdrücklich rechtliche Schritte im Falle der unverlangten Zusendung von Werbeinformationen, etwa durch Spam-E-Mails, vor.



[b]Newsletterdaten[/b]
Wenn Sie den auf der Webseite angebotenen Newsletter beziehen möchten, benötigen wir von Ihnen eine E-Mail-Adresse sowie Informationen, welche uns die Überprüfung gestatten, dass Sie der Inhaber der angegebenen E-Mail-Adresse sind und mit dem Empfang des Newsletters einverstanden sind. Weitere Daten werden nicht erhoben. Diese Daten verwenden wir ausschließlich für den Versand der angeforderten Informationen und geben sie nicht an Dritte weiter.
Die erteilte Einwilligung zur Speicherung der Daten, der E-Mail-Adresse sowie deren Nutzung zum Versand des Newsletters können Sie jederzeit widerrufen , etwa über den „Austragen“-Link im Newsletter.

[i]Quellenangabe: [url=http://www.e-recht24.de/muster-datenschutzerklaerung.html]eRecht24[/url][/i]');

// ABOUT - hyperclock
define('_INSTALL_ABOUT','[b][i]VoyaTrax Content Management System[/i] (vtCMS)[/b] ist ein dynamisches OO (Objekt Orientiertes) Open Source Portalskript geschrieben in PHP.
vtCMS ist somit ein ideales CMS für den Aufbau von kleineren und größeren Communities.
vtCMS ist freigegeben unter den Bedingungen der [url=http://www.gnu.org/copyleft/gpl.html]GNU General Public License (GPL)[/url] und ist frei zu verwenden und zu ändern. 
Es ist frei, so lange Änderungen, wie Sie durch die Bestimungen der GPL genannt sind, erhalten bleiben.');

// DISCLAIMER - hyperclock
define('_INSTALL_DISCLAIMER','[b]Haftung für Inhalte[/b]
Als Diensteanbieter sind wir gemäß § 7 Abs.1 TMG für eigene Inhalte auf diesen Seiten nach den allgemeinen Gesetzen verantwortlich. Nach §§ 8 bis 10 TMG sind wir als Diensteanbieter jedoch nicht verpflichtet, übermittelte oder gespeicherte fremde Informationen zu überwachen oder nach Umständen zu forschen, die auf eine rechtswidrige Tätigkeit hinweisen. Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach den allgemeinen Gesetzen bleiben hiervon unberührt. Eine diesbezügliche Haftung ist jedoch erst ab dem Zeitpunkt der Kenntnis einer konkreten Rechtsverletzung möglich. Bei Bekanntwerden von entsprechenden Rechtsverletzungen werden wir diese Inhalte umgehend entfernen. 


[b]Haftung für Links[/b]
Unser Angebot enthält Links zu externen Webseiten Dritter, auf deren Inhalte wir keinen Einfluss haben. Deshalb können wir für diese fremden Inhalte auch keine Gewähr übernehmen. Für die Inhalte der verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber der Seiten verantwortlich. Die verlinkten Seiten wurden zum Zeitpunkt der Verlinkung auf mögliche Rechtsverstöße überprüft. Rechtswidrige Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar. Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist jedoch ohne konkrete Anhaltspunkte einer Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Links umgehend entfernen.


[b]Urheberrecht[/b]
Die durch die Seitenbetreiber erstellten Inhalte und Werke auf diesen Seiten unterliegen dem deutschen Urheberrecht. Die Vervielfältigung, Bearbeitung, Verbreitung und jede Art der Verwertung außerhalb der Grenzen des Urheberrechtes bedürfen der schriftlichen Zustimmung des jeweiligen Autors bzw. Erstellers. Downloads und Kopien dieser Seite sind nur für den privaten, nicht kommerziellen Gebrauch gestattet. Soweit die Inhalte auf dieser Seite nicht vom Betreiber erstellt wurden, werden die Urheberrechte Dritter beachtet. Insbesondere werden Inhalte Dritter als solche gekennzeichnet. Sollten Sie trotzdem auf eine Urheberrechtsverletzung aufmerksam werden, bitten wir um einen entsprechenden Hinweis. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Inhalte umgehend entfernen.
Quelle: [url=http://www.e-recht24.de]http://www.e-recht24.de[/url]');

