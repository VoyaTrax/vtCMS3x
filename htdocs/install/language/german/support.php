<?php
// 
// _LANGCODE: en
// _CHARSET : UTF-8
// Translator: XOOPS Translation Team

$supports = array();

// Support site for English
$supports['english'] = array(
    'url' => 'http://www.xoops.eu/',
    'title' => 'English support');

// Add extra support sites, use corresponding language folder name as key, for instance:
/*
$supports["french"] = array(
    "url"   => "http://www.frxoops.org/",
    "title" => "Support francophone"
);
*/

// Support site for German
$supports["german"] = array(
    "url"   => "http://www.voyatrax.eu/",
    "title" => "Deutscher Support",
);
