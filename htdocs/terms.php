<?php
/**
 * XOOPS common initialization file
 *
 * You may not change or alter any portion of this comment or credits
 * of supporting developers from this source code or any supporting source code
 * which is considered copyrighted (c) material of the original comment or credit authors.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * @copyright       (c) 2000-2014 XOOPS Project (www.xoops.org)
 * @license GNU GPL 2 (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * @package kernel
 * @version $Id$
 */
include __DIR__ . '/mainfile.php';

include_once $GLOBALS['xoops']->path('header.php');
global $xoopsConfig, $xoopsUser;
if ($xoopsConfig['closesite'] == 1) { 
  $allowed = false;
  if (is_object($xoopsUser)) {
    foreach ($xoopsUser->getGroups() as $group) {
        if (in_array($group, $xoopsConfig['closesite_okgrp']) || XOOPS_GROUP_ADMIN == $group) {
            $allowed = true;
            break;
        }
    }
  }
  if (!$allowed) { 
    $GLOBALS['xoopsTpl']->assign( 'xoops_showlblock', 0 );
    $GLOBALS['xoopsTpl']->assign( 'xoops_showrblock', 0 ); 
    $GLOBALS['xoopsTpl']->assign( 'xoops_rblocks', array() );
    $GLOBALS['xoopsTpl']->assign( 'xoops_lblocks', array() );
    $GLOBALS['xoopsTpl']->assign( 'xoops_ccblocks', array() );
    $GLOBALS['xoopsTpl']->assign( 'xoops_crblocks', array() );
    $GLOBALS['xoopsTpl']->assign( 'xoops_clblocks', array() );
  }
}
include_once $GLOBALS['xoops']->path('/modules/system/constants.php');
$txtoutput = xoops_getConfigOption('terms', 'SYSTEM_CAT_TERMS');
$myts =& MyTextSanitizer::getInstance();
$txtoutput = $myts->displayTarea($txtoutput, 0, 1, 1, 1, 1);
//echo "<pre>" . $txtoutput . "</pre>";
?>
<!-- HTML CONTENT-->
<table width="100%" cellspacing="1" class="outer">
  <tr>
    <th><?php echo _TERMS; ?></th>
  </tr>
  <tr>
    <td class="odd" style="text-align:left;"><?php echo $txtoutput; ?></td>
  </tr>
</table>
<br>
<table width="100%" cellspacing="0" class="outer">
  <tr>
 <!--   <td class="foot" style="font-weight:normal;" align="right"><span class="itemPoster"><?php //echo _MI_COPYRIGHT; ?></span></td>-->
  </tr>
</table>
<br>
<!-- END HTML CONTENT-->
<?php
include_once $GLOBALS['xoops']->path('footer.php');
?>
