<?php

define('THEME_INFO', 'Info');
define('THEME_CLOSE', 'Schließen');

//Nav Menu
define('THEME_HOME', 'Home');
define('THEME_MODULE1', 'Über');
define('THEME_MODULE2', 'Nachrichten');
define('THEME_MODULE3', 'Forum');
define('THEME_MODULE4', 'Kontakt');
define('THEME_SEARCH_TEXT', 'Suche hier...');
define('THEME_SEARCH_BUTTON', 'Go');

//Slider
define('THEME_READMORE', 'Weiterlesen');

//Home Message
define('THEME_ABOUTUS', 'Über Uns');
define('THEME_LEARNINGMORE', 'Learning More...');

//NewBB
define('THEME_NEWBB_TOPIC', 'Mehr');
define('THEME_FORUM_SPONSORBY', 'Sponsor By.: ');
define('THEME_GOTOTHEFORUM', 'Zum Forum');
define('THEME_FORUM_DESCRIPTION', 'Beschreibung');
define('THEME_NEWBB_SEARCH_FORUM', 'Suche im Forum...');
define('THEME_NEWBB_SEARCH_TOPIC', 'Suche im Thema...');
define('THEME_FORUM_DESC', 'Über dieses Forum');
define('THEME_FORUM_NEWTOPIC', 'Neues Thema');
define('THEME_FORUM_REGISTER', 'Register');
define('THEME_FORUM_SEARCH', 'Suchen');
define('THEME_FORUM_ADVSEARCH', 'Erweiterte Suche');
define('THEME_FORUM_REPLY', 'Reply');
define('THEME_ADD_POLL', 'Umfrage hinzufügen');

//Block login
define('THEME_LOGIN', 'Dein Benutzername');
define('THEME_PASS', 'Ihr Passwort');

//Cookie consent -- escape for javascript if needed
define('THEME_COOKIE_MESSAGE', 'Diese Website verwendet Cookies, um sicherzustellen, dass Sie die beste Erfahrung auf unserer Website erhalten');
define('THEME_COOKIE_DISMISS', 'Hab\'s!');
define('THEME_COOKIE_LEARNMORE', 'Mehr info');

//Block User
define('THEME_USER_WELCOME', 'Willkommen');
