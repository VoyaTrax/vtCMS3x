<?php

//Nav Menu
define("THEME_HOME", "Homepage");
define("THEME_MODULE1", "Info");
define("THEME_MODULE2", "News");
define("THEME_MODULE3", "Foren");
define("THEME_MODULE4", "Kontakt");
define("THEME_SEARCH_TEXT", "Suche hier ...");
define("THEME_SEARCH_BUTTON", "Los");

//Slider
define("THEME_READMORE", "Mehr lesen");

//Home Message
define("THEME_ABOUTUS", "�ber Uns");
define("THEME_LEARNINGMORE", "Mehr zu erfahren...");

//NewBB
define("THEME_NEWBB_TOPIC", "Themen");
define("THEME_FORUM_SPONSORBY", "Sponsor By.: ");
define("THEME_GOTOTHEFORUM", "Gehe zum Forum");
define("THEME_FORUM_DESCRIPTION", "Beschreibung");
define("THEME_NEWBB_SEARCH_FORUM", "Suchen in Foren...");
define("THEME_NEWBB_SEARCH_TOPIC", "Suchen in Themen...");
define("THEME_FORUM_DESC", "�ber diese Foren");

//Block login
define("THEME_LOGIN", "Ihr Mitgliedsname");
define("THEME_PASS", "Ihr Passwort");
